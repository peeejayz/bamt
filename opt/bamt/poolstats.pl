#!/usr/bin/perl

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.
  
sub get_poollist
{
 return qw(abc bitclockers btcguild deepbit slush bithasher mtred ozcoin nofee rfc polmine btcpool triple mineco kiwi);
}

# pool specific - return current hashrate, confirmed rewards, unconfirmed rewards, estimated this round
  
sub getPoolStats_bitclockers
{
        my ($apikey) = @_;

        my $url = "http://bitclockers.com/api/$apikey";
        my ($confirm, $estimate, $hash);

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success) 
	{

                ($confirm, $hash, $estimate) = ($res->content =~ /.*?"balance":"(.*?)",.*?"hashrate":(.*?),.*?"estimatedearnings":"(.*?)",.*/);
		$hash = sprintf("%-3.1f",$hash);
        } 
	else 
	{
                $confirm = 'Failed';
        }
        return ($hash, $confirm, 'na', $estimate);
}


sub getPoolStats_deepbit
{
        my ($apikey) = @_;

        my $url = "http://deepbit.net/api/$apikey";
        my ($confirm, $estimate, $hash);

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success)
        {

                ($confirm, $hash) = ($res->content =~ /.*?"confirmed_reward":(.*?),.*?"hashrate":(.*?),.*/);
		$hash = sprintf("%-3.1f",$hash);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, 'na', 'na');
}


sub getPoolStats_btcguild 
{
	my ($apikey) = @_;

        my $url = "http://www.btcguild.com/api.php?api_key=$apikey";
        my ($confirm, $unconfirm, $estimate, $day, $hash);

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success) 
	{
                ($confirm, $unconfirm, $estimate, $day, $hash) = ($res->content =~ /.*?"confirmed_rewards":(.*?),.*?"unconfirmed_rewards":(.*?),.*?"estimated_rewards":(.*?),.*?"24hour_rewards":(.*?),.*?"hash_rate":(.*?),.*/);
         	$hash = sprintf("%-3.1f",$hash);
	}
	else 
	{
                $confirm = 'Failed';
        }
        return ($hash, $confirm, $unconfirm, $estimate);
}



sub getPoolStats_slush
{
        my ($apikey) = @_;

        my $url = "http://mining.bitcoin.cz/accounts/profile/json/$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success) 
	{
                ($unconfirm, $confirm, $hash, $estimate) = ($res->content =~ /.*?"unconfirmed_reward": "(.*?)",.*?"confirmed_reward": "(.*?)",.*?"hashrate": (.*?)[,}].*?"estimated_reward": "(.*?)".*/);
		$hash = sprintf("%-3.1f",$hash);
        } 
	else 
	{
                $confirm = 'Failed';
        }
        return ($hash, $confirm, $unconfirm, $estimate);
}

sub getPoolStats_bithasher
{
        my ($apikey) = @_;

        my $url = "http://www.bithasher.com/index.php/ws/uStats/$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($hash) = ($res->content =~ /.*?hashrate":(\d+),.*/);
                $hash = sprintf("%-3.1f",$hash / 1000 / 1000);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, "na", "na");
}


sub getPoolStats_mtred
{
        my ($apikey) = @_;

        my $url = "https://mtred.com/api/user/key/$apikey/";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($confirm,$unconfirm,$hash) = ($res->content =~ /.*?balance\":\"([\d\.]+?)\",\"unconfirmed\":\"([\d\.]+?)\",.*\"mhash\":([\d\.]+?),.*/);
                $hash = sprintf("%-3.1f",$hash);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, $unconfirm, "na");
}



sub getPoolStats_ozcoin
{
        my ($apikey) = @_;

        my $url = "http://ozco.in/api.php?api_key=$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($confirm,$hash) = ($res->content =~ /.*confirmed_rewards\":\"([\d\.]+?)\",\"hashrate\":\"([\d\.]+?)\",.*/);
                $hash = sprintf("%-3.1f",$hash);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, "na", "na");
}


sub getPoolStats_nofee
{
        my ($apikey) = @_;

        my $url = "https://www.nofeemining.com/api.php?key=$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($estimate,$unconfirm,$confirm,$hash) = ($res->content =~ /.*\"estimated_reward\":([\d\.]+?),\"unconfirmed_reward\":([\d\.]+?),\"confirmed_reward\":([\d\.]+),.*\"roundShares\".*\"hashRate\":([\d\.]+)}.*/);
                $hash = sprintf("%-3.1f",$hash);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, $unconfirm, $estimate);
}



sub getPoolStats_polmine
{
        my ($apikey) = @_;

        my $url1 = "https://polmine.pl/?action=api&cmd=$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($confirm,$hash) = ($res->content =~ /.*\{\"balance\":([\d\.]+),.*hashrate\":\"([\d\.]+?)\",.*/);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, "na", "na");
}



sub getPoolStats_triple
{
        my ($apikey) = @_;

        my $url1 = "http://api.triplemining.com/json/$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {

                ($confirm,$hash) = ($res->content =~ /.*\{\"confirmed_reward\":\"([\d\.]+)\",\"hashrate\":([\d\.]+),.*/);
	
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, "na", "na");
}



sub getPoolStats_btcpool
{
        my ($apikey) = @_;

        my $url1 = "http://www.btcpool24.com/api.php?api_key=$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {

                ($confirm,$hash) = ($res->content =~ /.*\{\"confirmed_rewards\":\"([\d\.]+)\",\"hashrate\":\"([\d\.]+)\",.*/);
	
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, "na", "na");
}


sub getPoolStats_rfc
{
        my ($apikey) = @_;

        my $url1 = "https://www.rfcpool.com/api/user/account?key=$apikey";
	my $url2 = "https://www.rfcpool.com/api/user/workers?key=$apikey";
        my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($unconfirm,$confirm,$estimate) = ($res->content =~ /.*\{\"unconfirmed\":([\d\.]+),\"confirmed\":([\d\.]+)}.*\"estimated_reward\":([\d\.]+?),.*/);
		
		$req = HTTP::Request->new(GET => $url2);
        	$res = $ua->request($req);
        	if ($res->is_success)
        	{
			($hash) = ($res->content =~ /.*,\"hashrate\":\"([\d\.]+?)\".*/);
	                $hash = sprintf("%-3.1f",$hash);			
		}
	
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, $unconfirm, $estimate);
}


sub getPoolStats_abc
{
        my ($apikey) = @_;

        my $url1 = "http://www.abcpool.co/api.php?api_key=$apikey";
	  my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";
        $hash = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($confirm,$hash) = ($res->content =~ /.*\"confirmed_rewards\":\"([\d\.]+)\",\"hashrate\":\"([\d\.]+)\".*/);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, 'na', 'na');
}

sub getPoolStats_mineco
{
        my ($apikey) = @_;

        my $url1 = 'https://mineco.in/users/' . $apikey . '.json';
	my ($confirm, $unconfirm, $estimate, $hash);

        $confirm = "na";
        $hash = "na";

        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($estimate,$confirm,$unconfirm,$hash) = ($res->content =~ /.*\"estimated_reward_this_round\":\"?([\d\.]+)\"?,\"confirmed_reward\":\"?([\d\.]+)\"?,\"unconfirmed_reward\":\"?([\d\.]+)\"?,.*\"hash_rate\":([\d\.]+),.*/);
                $hash = sprintf("%-3.1f",$hash / 1000000);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, $confirm, $unconfirm, $estimate);
}


sub getPoolStats_kiwi
{
        my ($apikey) = @_;

        my $url1 = 'http://www.kiwipool.me/json/' . $apikey;
	my ($confirm, $unconfirm, $estimate, $hash);

        $hash = "na";
        
        
        my $ua = LWP::UserAgent->new;
        $ua->agent('bamt/0.1');
        my $req = HTTP::Request->new(GET => $url1);
        my $res = $ua->request($req);
        if ($res->is_success)
        {
                ($hash) = ($res->content =~ /.*\"hashrate\":([\d\.]+),.*/);
                $hash = sprintf("%-3.1f",$hash);
        }
        else
        {
                $confirm = 'Failed';
        }
        return ($hash, 'na', 'na', 'na');
}



1;
